/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-28 10:16:02
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 13:29:01
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(Function)
export class FunBaseExtends {
}
/**扩展组件实例方法 */
@ExtendsLoad(Function.prototype)
export class FunExtends {
}