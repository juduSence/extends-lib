
/*
* @features: 主要功能
* @description: 内容说明
* @Date: 2021-09-28 10:12:45
* @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-22 10:38:37
 * @LastEditors: zhangjiajun
*/

import SqlUtil from "../../Storage/SqlUtil";
import { ExtendsLoad } from "../CCExtends";
/**扩展原型组件 */
@ExtendsLoad(Set)
export class SetBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(Set.prototype)
export class SetExtends {
    /**
     * 根据key保存当前数值到内存中
     * @param saveStr 要存储的key
     */
    static saveSet(saveStr: string) {
        let set = this as any as Set<any>;
        return SqlUtil.setItem(saveStr, set.transArr());
    }

    /**
     * 根据key从内存中加载指定值到当前数组中
     * @param saveStr 要存储的key
     * @param def 默认值
     * @param isClear 是否清除数组已有所有值，默认不清除
     * @returns 
     */
    static reloadSet(saveStr: string, def = new Set(), isClear = false) {
        try {
            let set = this as any as Set<any>;
            let getArr = SqlUtil.getItem(saveStr, def) as [any][];
            if (isClear) {
                set.clear();
            }
            set.addList(getArr);
            return true;
        } catch (error) {
            return false;
        }
    }
    /**
     * 把当前set中的元素转成arr
     * @returns 
     */
    static transArr() {
        let set = this as any as Set<any>;
        return Array.from(set);
    }

    /**
     * 添加指定元素列表到当前数组中,会改变当前set结构
     * @param arrList 要添加的元素列表
     * @example 
     * var a = new Set();
     * var b = new Set([1]);
     * a.addList(b)  => Set(1) {1}
     * 
     * var arr = [2,3]
     * a.addList(arr) => Set(3) {1,2,3}
     */
    static addList(arrList: [any][] | Set<any>) {
        let set = this as any as Set<any>;
        arrList.forEach(arr => {
            set.add(arr);
        })
    }

}
