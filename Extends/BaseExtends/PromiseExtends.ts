/*
 * @features: 功能
 * @description: 说明
 * @Date: 2022-04-17 16:13:07
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-09-27 01:19:29
 * @LastEditors: judu233
 */
import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(Promise)
export class PromiseBaseExtends {
    /**
    * 异步等待 - setTimeout
    * @param seconds -秒
    */
    static wait(seconds = 0): Promise<void> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(null);
            }, seconds * 1000);
        });
    }

    /**
     * 判断变量是否是promise
     * @param val 要判断的变量值
     * @returns 
     */
    static isPromise(val: any): val is Promise<any> {
        // return p && Object.prototype.toString.call(p) === "[object Promise]";
        return val && (<Promise<any>>val).then !== undefined;
    }
}
/**扩展组件实例方法 */
@ExtendsLoad(Promise.prototype)
export class PromiseExtends {
    /**
    * 异步等待 - setTimeout
    * @param seconds  秒
    */
    static async wait(seconds: number): Promise<void> {
        let promise = this as any as Promise<void>;
        await promise;
        return await new Promise(resolve => {
            setTimeout(() => {
                resolve(null);
            }, seconds * 1000);
        });
    }
}
