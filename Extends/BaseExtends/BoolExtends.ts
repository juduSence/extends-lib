/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-28 10:14:09
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 12:17:59
 * @LastEditors: judu233
 */

import SqlUtil from "../../Storage/SqlUtil";
import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(Boolean)
export class BoolBaseExtends {
    /**
     * 随机true或false;
     * @returns bool
     */
    static random() {
        return Math.random() > 0.5;
    }
}
/**扩展组件实例方法 */
@ExtendsLoad(Boolean.prototype)
export class BoolExtends {
    /**
     * 存储当前的数值
     * @param saveStr 要存储的key
     * @param elmName 数值对应元素的名字(注意不要重复)
     */
    static saveBool(saveStr: string, elmName: string) {
        let bool = this as any as boolean;
        let isSave = SqlUtil.setItem(saveStr, bool);
        if (isSave)
            SqlUtil._setOriginalName(saveStr + '_boolElmName', elmName);
        else
            console.log(`存储OriginalName失败,key:${saveStr}`);
        return isSave;
    }
    /**
     * 加载存储的数值到指定目标
     * @param target 要加载的目标（数值所在的对象）
     * @param saveStr 要加载的key
     * @param def 默认值
     */
    static reloadBool(target: any, saveStr: string, def = true) {
        try {
            let name = SqlUtil._getOriginalName(saveStr + '_boolElmName');
            if (name != null) {
                if (target && target[name] != null) {
                    target[name] = SqlUtil.getItem(saveStr, def);
                    return true;
                } else
                    console.warn(`要加载的数值目标不存在或者未创建`);
            } else
                console.warn(`要加载的数值目标 _boolElmName 不存在`);
            return false;
        } catch (error) {
            return false;
        }
    }
}
