import { AnimationBaseExtends,AnimationeExtends } from "./BaseComponentExtends/AnimationExtends";
import { ButtonBaseExtends,ButtoneExtends } from "./BaseComponentExtends/ButtonExtends";
import { CameraBaseExtends,CameraExtends } from "./BaseComponentExtends/CameraExtends";
import { ColorBaseExtends,ColorExtends } from "./BaseComponentExtends/ColorExtends";
import { ComponentBaseExtends,ComponentExtends } from "./BaseComponentExtends/ComponentExtends";
import { DirectorBaseExtends,DirectorExtends } from "./BaseComponentExtends/DirectorExtends";
import { ComponentEventHandleBaseExtends,ComponentEventHandleExtends } from "./BaseComponentExtends/EventHandleExtends";
import { GraphicsBaseExtends,GraphicsExtends } from "./BaseComponentExtends/GraphicsExtends";
import { LabelBaseExtends,LabelExtends } from "./BaseComponentExtends/LabelExtends";
import { NodeBaseExtends,NodeExtends } from "./BaseComponentExtends/NodeExtends";
import { RectBaseExtends,RectExtends } from "./BaseComponentExtends/RectExtends";
import { RichTextBaseExtends,RichTexteExtends } from "./BaseComponentExtends/RitchTextExtends";
import { SizeBaseExtends,SizeExtends } from "./BaseComponentExtends/SizeExtends";
import { SkeletonBaseExtends,SkeletonExtends } from "./BaseComponentExtends/SkeletonExtends";
import { SpriteBaseExtends,SpriteExtends } from "./BaseComponentExtends/SpriteExtends";
import { Texture2DBaseExtends,Texture2DExtends } from "./BaseComponentExtends/Texture2DExtends";
import { TweenBaseExtends,TweenExtends } from "./BaseComponentExtends/TweenExtends";
import { WidgetBaseExtends,WidgetExtends } from "./BaseComponentExtends/WidgetExtends";
import { ArrayBaseExtends,ArrayExtends } from "./BaseExtends/ArrayExtends";
import { BoolBaseExtends,BoolExtends } from "./BaseExtends/BoolExtends";
import { DateConstructorExtends,DateExtends } from "./BaseExtends/DataExtends";
import { FunBaseExtends,FunExtends } from "./BaseExtends/FuncExtends";
import { MapBaseExtends,MapExtends } from "./BaseExtends/MapExtends";
import { NumberBaseExtends,NumberExtends } from "./BaseExtends/NumberExtends";
import { ObjectBaseExtends,ObjectExtends } from "./BaseExtends/ObjectExtends";
import { PromiseBaseExtends,PromiseExtends } from "./BaseExtends/PromiseExtends";
import { RegExpBaseExtends,RegExpExtends } from "./BaseExtends/RegExpExtends";
import { SetBaseExtends,SetExtends } from "./BaseExtends/SetExtends";
import { StringBaseExtends,StringExtends } from "./BaseExtends/StringExtends";
import { Mat3BaseExtends,Mat3Extends } from "./MathExtends/Mat3Extends";
import { Mat4BaseExtends,Mat4Extends } from "./MathExtends/Mat4Extends";
import { MathExtends, } from "./MathExtends/MathExtends";
import { quatBaseExtends,quatExtends } from "./MathExtends/QuatExtends";
import { Vec2BaseExtends,Vec2Extends } from "./MathExtends/Vect2Extends";
import { Vec3BaseExtends,Vec3Extends } from "./MathExtends/Vect3Extends";
import { Vec4BaseExtends,Vec4Extends } from "./MathExtends/Vect4Extends";
declare global {
export module cc.Node {
let createButton:typeof NodeBaseExtends.createButton;
let createSprite:typeof NodeBaseExtends.createSprite;
let clearChildren:typeof NodeBaseExtends.clearChildren;
let createNode:typeof NodeBaseExtends.createNode;
}
export module cc.Rect {
let overlapRect:typeof RectBaseExtends.overlapRect;
}
export module cc.Sprite {
let base64ToTexture:typeof SpriteBaseExtends.base64ToTexture;
let base64ToBlob:typeof SpriteBaseExtends.base64ToBlob;
let createSplashSprite:typeof SpriteBaseExtends.createSplashSprite;
}
export module cc.Tween {
let fadeTo:typeof TweenBaseExtends.fadeTo;
let fadeOut:typeof TweenBaseExtends.fadeOut;
let fadeIn:typeof TweenBaseExtends.fadeIn;
let scaleTo:typeof TweenBaseExtends.scaleTo;
let scaleBy:typeof TweenBaseExtends.scaleBy;
let moveTo:typeof TweenBaseExtends.moveTo;
let moveBy:typeof TweenBaseExtends.moveBy;
let node2Link:typeof TweenBaseExtends.node2Link;
let animPlayList:typeof TweenBaseExtends.animPlayList;
let animPromise:typeof TweenBaseExtends.animPromise;
let playSquAnimList:typeof TweenBaseExtends.playSquAnimList;
let link:typeof TweenBaseExtends.link;
let buttonAnim:typeof TweenBaseExtends.buttonAnim;
let breatheAnim:typeof TweenBaseExtends.breatheAnim;
let winHide:typeof TweenBaseExtends.winHide;
let winShow:typeof TweenBaseExtends.winShow;
let rotate:typeof TweenBaseExtends.rotate;
let flip:typeof TweenBaseExtends.flip;
let roll_num:typeof TweenBaseExtends.roll_num;
let playBounceMoveTween:typeof TweenBaseExtends.playBounceMoveTween;
let playBounceScaleTween:typeof TweenBaseExtends.playBounceScaleTween;
let playJellyTween:typeof TweenBaseExtends.playJellyTween;
}
export module cc.Vec3 {
let progress:typeof Vec3BaseExtends.progress;
let slerp:typeof Vec3BaseExtends.slerp;
let rotateTo:typeof Vec3BaseExtends.rotateTo;
}

export module cc{
export interface Animation {
restAnim:typeof AnimationeExtends.restAnim;
asyncPlayAnim:typeof AnimationeExtends.asyncPlayAnim;
}
export interface Button {
createInteractionEvent:typeof ButtoneExtends.createInteractionEvent;
copyHandleTo:typeof ButtoneExtends.copyHandleTo;
_applyTransition:typeof ButtoneExtends._applyTransition;
}
export interface Camera {
kawaseBg:typeof CameraExtends.kawaseBg;
getRenderTexture:typeof CameraExtends.getRenderTexture;
renderWithMaterial:typeof CameraExtends.renderWithMaterial;
screenShot:typeof CameraExtends.screenShot;
}
export interface Component {
getComName:typeof ComponentExtends.getComName;
getSName:typeof ComponentExtends.getSName;
waitCmpt:typeof ComponentExtends.waitCmpt;
}
export interface Graphics {
createRound:typeof GraphicsExtends.createRound;
createRectangle:typeof GraphicsExtends.createRectangle;
}
export interface Label {
measureSize:typeof LabelExtends.measureSize;
}
export interface Node {
addChildBind:typeof NodeExtends.addChildBind;
addComponentBind:typeof NodeExtends.addComponentBind;
setInfo:typeof NodeExtends.setInfo;
converNodeToOtherNodeSpaceAR:typeof NodeExtends.converNodeToOtherNodeSpaceAR;
converNodeToWorldSpaceAR:typeof NodeExtends.converNodeToWorldSpaceAR;
convertNodeToWorldSpaceAR:typeof NodeExtends.convertNodeToWorldSpaceAR;
converTouchPointToNode:typeof NodeExtends.converTouchPointToNode;
regTouchStartClick:typeof NodeExtends.regTouchStartClick;
regTouchEndClick:typeof NodeExtends.regTouchEndClick;
regTouchEndClickChild:typeof NodeExtends.regTouchEndClickChild;
unRegTouchStartClick:typeof NodeExtends.unRegTouchStartClick;
unRegTouchEndClick:typeof NodeExtends.unRegTouchEndClick;
regMoveFollow:typeof NodeExtends.regMoveFollow;
regMove:typeof NodeExtends.regMove;
setSwallow:typeof NodeExtends.setSwallow;
forEachChildren:typeof NodeExtends.forEachChildren;
findN:typeof NodeExtends.findN;
createHandle:typeof NodeExtends.createHandle;
restPos:typeof NodeExtends.restPos;
setSf:typeof NodeExtends.setSf;
setLb:typeof NodeExtends.setLb;
restAll:typeof NodeExtends.restAll;
flipX:typeof NodeExtends.flipX;
flipY:typeof NodeExtends.flipY;
set_node_world:typeof NodeExtends.set_node_world;
checkPercent:typeof NodeExtends.checkPercent;
computRectJoinUnion:typeof NodeExtends.computRectJoinUnion;
getAllChildNodeAndCom:typeof NodeExtends.getAllChildNodeAndCom;
getIndexByParent:typeof NodeExtends.getIndexByParent;
getFullPath:typeof NodeExtends.getFullPath;
}
export interface Rect {
overlapRect:typeof RectExtends.overlapRect;
toSize:typeof RectExtends.toSize;
toV2:typeof RectExtends.toV2;
toV3:typeof RectExtends.toV3;
toV4:typeof RectExtends.toV4;
print:typeof RectExtends.print;
converPolygon:typeof RectExtends.converPolygon;
getLimit:typeof RectExtends.getLimit;
leftTopEqually:typeof RectExtends.leftTopEqually;
}
export interface Size {
toV2:typeof SizeExtends.toV2;
toV3:typeof SizeExtends.toV3;
toV4:typeof SizeExtends.toV4;
toRect:typeof SizeExtends.toRect;
multiply:typeof SizeExtends.multiply;
}
export interface Sprite {
loadRemoteImage:typeof SpriteExtends.loadRemoteImage;
loadImage:typeof SpriteExtends.loadImage;
getPixelColor:typeof SpriteExtends.getPixelColor;
imageToBase64:typeof SpriteExtends.imageToBase64;
setSf:typeof SpriteExtends.setSf;
setSfByAtlas:typeof SpriteExtends.setSfByAtlas;
setSpMtGray:typeof SpriteExtends.setSpMtGray;
setSpMtNormal:typeof SpriteExtends.setSpMtNormal;
setSplashSprite:typeof SpriteExtends.setSplashSprite;
cropSf:typeof SpriteExtends.cropSf;
}
export interface Texture2D {
getPixelColor:typeof Texture2DExtends.getPixelColor;
}
export interface Tween {
createBezierTo:typeof TweenExtends.createBezierTo;
}
export interface Widget {
restAllZero:typeof WidgetExtends.restAllZero;
restAllCenter:typeof WidgetExtends.restAllCenter;
}

export namespace math {
let progress:typeof MathExtends.progress;
let lerpAngle:typeof MathExtends.lerpAngle;
let decimal:typeof MathExtends.decimal;
let angleTowards:typeof MathExtends.angleTowards;
let zeroEqually:typeof MathExtends.zeroEqually;
let equally:typeof MathExtends.equally;
let outOrInTriangle:typeof MathExtends.outOrInTriangle;
let getAngle:typeof MathExtends.getAngle;
let lineLine:typeof MathExtends.lineLine;
let lineRect:typeof MathExtends.lineRect;
let linePolygon:typeof MathExtends.linePolygon;
let rectRect:typeof MathExtends.rectRect;
let rectPolygon:typeof MathExtends.rectPolygon;
let polygonPolygon:typeof MathExtends.polygonPolygon;
let pointInCircle:typeof MathExtends.pointInCircle;
let circleCircle:typeof MathExtends.circleCircle;
let polygonCircle:typeof MathExtends.polygonCircle;
let pointInPolygon:typeof MathExtends.pointInPolygon;
let pointLineDistance:typeof MathExtends.pointLineDistance;
let getRectRotatePoints:typeof MathExtends.getRectRotatePoints;
}
export interface Vec2 {
toV3:typeof Vec2Extends.toV3;
toV4:typeof Vec2Extends.toV4;
toSize:typeof Vec2Extends.toSize;
toRect:typeof Vec2Extends.toRect;
print:typeof Vec2Extends.print;
outOrInTriangle:typeof Vec2Extends.outOrInTriangle;
negationX:typeof Vec2Extends.negationX;
negationY:typeof Vec2Extends.negationY;
compresseX:typeof Vec2Extends.compresseX;
compresseY:typeof Vec2Extends.compresseY;
}
export interface Vec3 {
mtimes:typeof Vec3Extends.mtimes;
plus:typeof Vec3Extends.plus;
toV2:typeof Vec3Extends.toV2;
toV4:typeof Vec3Extends.toV4;
toSize:typeof Vec3Extends.toSize;
toRect:typeof Vec3Extends.toRect;
negationX:typeof Vec3Extends.negationX;
negationY:typeof Vec3Extends.negationY;
negationZ:typeof Vec3Extends.negationZ;
compresseX:typeof Vec3Extends.compresseX;
compresseY:typeof Vec3Extends.compresseY;
compresseZ:typeof Vec3Extends.compresseZ;
print:typeof Vec3Extends.print;
}
export interface Vec4 {
toV2:typeof Vec4Extends.toV2;
toV3:typeof Vec4Extends.toV3;
toRect:typeof Vec4Extends.toRect;
toSize:typeof Vec4Extends.toSize;
negationX:typeof Vec4Extends.negationX;
negationY:typeof Vec4Extends.negationY;
negationZ:typeof Vec4Extends.negationZ;
negationW:typeof Vec4Extends.negationW;
compresseX:typeof Vec4Extends.compresseX;
compresseY:typeof Vec4Extends.compresseY;
compresseZ:typeof Vec4Extends.compresseZ;
compresseW:typeof Vec4Extends.compresseW;
print:typeof Vec4Extends.print;
}
}
export module cc.Component.EventHandler {
let createHandle:typeof ComponentEventHandleBaseExtends.createHandle;
}

export module cc.Component {
export interface EventHandler {
copyHandle:typeof ComponentEventHandleExtends.copyHandle;
checkHandle:typeof ComponentEventHandleExtends.checkHandle;
emitAsync:typeof ComponentEventHandleExtends.emitAsync;
}

}

export module sp {
export interface Skeleton {
loadRemoteSkeleton:typeof SkeletonExtends.loadRemoteSkeleton;
loadSkeleton:typeof SkeletonExtends.loadSkeleton;
reloadSpAnimation:typeof SkeletonExtends.reloadSpAnimation;
}

}
export interface ArrayConstructor {
copy2d:typeof ArrayBaseExtends.copy2d;
autoCompletion:typeof ArrayBaseExtends.autoCompletion;
}
export interface Array<T> {
saveArr:typeof ArrayExtends.saveArr;
reloadArr:typeof ArrayExtends.reloadArr;
fisherYatesShuffle:typeof ArrayExtends.fisherYatesShuffle;
confound:typeof ArrayExtends.confound;
arrayRand:typeof ArrayExtends.arrayRand;
getRandomValueInArray:typeof ArrayExtends.getRandomValueInArray;
flattening:typeof ArrayExtends.flattening;
combineArr:typeof ArrayExtends.combineArr;
exchangeNode:typeof ArrayExtends.exchangeNode;
hasNull:typeof ArrayExtends.hasNull;
hasUndf:typeof ArrayExtends.hasUndf;
arraySwap:typeof ArrayExtends.arraySwap;
arrayHas:typeof ArrayExtends.arrayHas;
arrayMove:typeof ArrayExtends.arrayMove;
arrayAdd:typeof ArrayExtends.arrayAdd;
arrayDelete:typeof ArrayExtends.arrayDelete;
getElmCount:typeof ArrayExtends.getElmCount;
transMap:typeof ArrayExtends.transMap;
transSet:typeof ArrayExtends.transSet;
overrideArr:typeof ArrayExtends.overrideArr;
last:typeof ArrayExtends.last;
first:typeof ArrayExtends.first;
selObjArr:typeof ArrayExtends.selObjArr;
arrSort:typeof ArrayExtends.arrSort;
}
export interface BooleanConstructor {
random:typeof BoolBaseExtends.random;
}
export interface Boolean {
saveBool:typeof BoolExtends.saveBool;
reloadBool:typeof BoolExtends.reloadBool;
}
export interface DateConstructor {
timeNow:typeof DateConstructorExtends.timeNow;
timeNowMillisecons:typeof DateConstructorExtends.timeNowMillisecons;
getDateList:typeof DateConstructorExtends.getDateList;
}
export interface Date {
saveDate:typeof DateExtends.saveDate;
reloadDate:typeof DateExtends.reloadDate;
format:typeof DateExtends.format;
checkIsWorkDay:typeof DateExtends.checkIsWorkDay;
isVaild:typeof DateExtends.isVaild;
checkIsSameDay:typeof DateExtends.checkIsSameDay;
isSameWeek:typeof DateExtends.isSameWeek;
isSameWeek2:typeof DateExtends.isSameWeek2;
isSameWeek3:typeof DateExtends.isSameWeek3;
getNextDayDate:typeof DateExtends.getNextDayDate;
getNextDayRemaining:typeof DateExtends.getNextDayRemaining;
}
export interface Map<K, V>  {
saveMap:typeof MapExtends.saveMap;
reloadMap:typeof MapExtends.reloadMap;
transValueArr:typeof MapExtends.transValueArr;
transKeyArr:typeof MapExtends.transKeyArr;
transArr:typeof MapExtends.transArr;
addList:typeof MapExtends.addList;
}
export interface NumberConstructor {
inRange:typeof NumberBaseExtends.inRange;
randInt:typeof NumberBaseExtends.randInt;
randFloat:typeof NumberBaseExtends.randFloat;
}
export interface Number {
saveNum:typeof NumberExtends.saveNum;
reloadNum:typeof NumberExtends.reloadNum;
toNum:typeof NumberExtends.toNum;
dealValue:typeof NumberExtends.dealValue;
}
export interface ObjectConstructor {
getNew:typeof ObjectBaseExtends.getNew;
clonDeepObj:typeof ObjectBaseExtends.clonDeepObj;
copyObj:typeof ObjectBaseExtends.copyObj;
isObject:typeof ObjectBaseExtends.isObject;
type:typeof ObjectBaseExtends.type;
transTypeToStr:typeof ObjectBaseExtends.transTypeToStr;
getVarType:typeof ObjectBaseExtends.getVarType;
flattenObject:typeof ObjectBaseExtends.flattenObject;
}
export interface Object {
saveObj:typeof ObjectExtends.saveObj;
reloadObj:typeof ObjectExtends.reloadObj;
isSave:typeof ObjectExtends.isSave;
margen:typeof ObjectExtends.margen;
deepFilter:typeof ObjectExtends.deepFilter;
dealNull:typeof ObjectExtends.dealNull;
removeEmptyField:typeof ObjectExtends.removeEmptyField;
flattenObject:typeof ObjectExtends.flattenObject;
getObjectValue:typeof ObjectExtends.getObjectValue;
}
export interface PromiseConstructor {
wait:typeof PromiseBaseExtends.wait;
isPromise:typeof PromiseBaseExtends.isPromise;
}
export interface Promise<T>  {
wait:typeof PromiseExtends.wait;
}
export interface RegExpConstructor {
getFileName:typeof RegExpBaseExtends.getFileName;
isNumber:typeof RegExpBaseExtends.isNumber;
}
export interface Set<T> {
saveSet:typeof SetExtends.saveSet;
reloadSet:typeof SetExtends.reloadSet;
transArr:typeof SetExtends.transArr;
addList:typeof SetExtends.addList;
}
export interface StringConstructor {
checkHas:typeof StringBaseExtends.checkHas;
isEmpty:typeof StringBaseExtends.isEmpty;
}
export interface String {
saveStr:typeof StringExtends.saveStr;
reloadStr:typeof StringExtends.reloadStr;
format:typeof StringExtends.format;
checkHas:typeof StringExtends.checkHas;
insertStr:typeof StringExtends.insertStr;
transReturn:typeof StringExtends.transReturn;
insertStrByCharator:typeof StringExtends.insertStrByCharator;
removeSpace:typeof StringExtends.removeSpace;
base64ToTexture:typeof StringExtends.base64ToTexture;
base64ToBlob:typeof StringExtends.base64ToBlob;
shuffleString:typeof StringExtends.shuffleString;
dealValue:typeof StringExtends.dealValue;
}
}