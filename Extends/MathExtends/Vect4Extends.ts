/*
 * @features: 功能
 * @description: 说明
 * @Date: 2021-09-13 23:49:48
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-23 22:37:44
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Vec4)
export class Vec4BaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Vec4.prototype)
export class Vec4Extends {

    /**把当前vect4转换成vect2, 会舍弃z和w */
    static toV2() {
        let v = this as any as cc.Vec4;
        return new cc.Vec2(v.x, v.y);
    }

    /**把当前vect4转换成vect3, 舍弃w */
    static toV3() {
        let v = this as any as cc.Vec4;
        return new cc.Vec3(v.x, v.y, v.z);
    }

    /**把当前vect4转换成Rect */
    static toRect() {
        let v = this as any as cc.Vec4;
        return new cc.Rect(v.x, v.y, v.z, v.w);
    }

    static toSize() {
        let v = this as any as cc.Vec4;
        return new cc.Size(v.x, v.y);
    }

    /** x取反 */
    static negationX() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(-p.x, p.y, p.z, p.w);
    }

    /** y取反 */
    static negationY() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(p.x, -p.y, p.z, p.w);
    }

    /** z取反 */
    static negationZ() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(p.x, p.y, - p.z, p.w);
    }

    /** w取反 */
    static negationW() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(p.x, p.y, p.z, -p.w);
    }

    /**压缩x轴为0 */
    static compresseX() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(0, p.y, p.z, p.w);
    }
    
    /**压缩y轴为0 */
    static compresseY() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(p.x, 0, p.z, p.w);
    }

    /**压缩z轴为0 */
    static compresseZ() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(p.x, p.y, 0, p.w);
    }
    /**压缩w轴为0 */
    static compresseW() {
        let p: cc.Vec4 = this as any;
        return new cc.Vec4(p.x, p.y, p.z, 0);
    }

    /**
     * 返回当前矩形的字符串表示。
     * @returns 
     */
    static print() {
        let self = this as any as cc.Vec4;
        return `(${self.x.toFixed(2)}, ${self.y.toFixed(2)}, ${self.z.toFixed(2)}, ${self.w.toFixed(2)})`;
    }
}