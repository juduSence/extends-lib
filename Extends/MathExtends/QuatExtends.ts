/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-28 10:36:04
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-12 17:15:52
 * @LastEditors: zhangjiajun
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Quat)
export class quatBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Quat.prototype)
export class quatExtends {

}