/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-28 10:36:54
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-12 17:14:11
 * @LastEditors: zhangjiajun
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Mat3)
export class Mat3BaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Mat3.prototype)
export class Mat3Extends {

}