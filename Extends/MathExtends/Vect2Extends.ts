/*
 * @features: 功能
 * @description: 说明
 * @Date: 2021-09-30 22:53:51
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-23 22:31:41
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Vec2)
export class Vec2BaseExtends {
}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Vec2.prototype)
export class Vec2Extends {
    /**把当前vect转换成vect3, 不足的填充0 */
    static toV3() {
        let v = this as any as cc.Vec2;
        return cc.v3(v.x, v.y);
    }

    /**把当前vect转换成vect4, 不足的填充0 */
    static toV4() {
        let v = this as any as cc.Vec2;
        return new cc.Vec4(v.x, v.y);
    }

    /**把当前vect转换成Size */
    static toSize() {
        let v = this as any as cc.Vec2;
        return new cc.Size(v.x, v.y);
    }
    static toRect() {
        let v = this as any as cc.Vec3;
        return new cc.Rect(v.x, v.y);
    }

    /**
     * 返回当前矩形的字符串表示。
     * @returns 
     */
    static print() {
        let self = this as any as cc.Vec2;
        return `(${self.x.toFixed(2)}, ${self.y.toFixed(2)})`;
    }

    /**
     * 判断当前的点在指定的p1，p2之上还是之下
     * 0 在线上， 大于0在下，小于0在上 
     * @param p1 
     * @param p2 
     * @returns 
     */
    static outOrInTriangle(p1: cc.Vec2, p2: cc.Vec2 = cc.v2(0, 0)) {
        let p: cc.Vec2 = this as any;
        let a = p2.y - p1.y;
        let b = p1.x - p2.x;
        let c = p2.x * p1.y - p1.x * p2.y;
        let result = a * p.x + b * p.y + c;
        return result;
    }

    /** x取反 */
    static negationX() {
        let p: cc.Vec2 = this as any;
        return cc.v2(-p.x, p.y);
    }

    /** y取反 */
    static negationY() {
        let p: cc.Vec2 = this as any;
        return cc.v2(p.x, -p.y);
    }

    /**压缩x轴为0 */
    static compresseX() {
        let p: cc.Vec2 = this as any;
        return cc.v2(0, p.y);
    }

    /**压缩y轴为0 */
    static compresseY() {
        let p: cc.Vec2 = this as any;
        return cc.v2(p.x, 0);
    }
}