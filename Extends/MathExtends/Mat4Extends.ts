/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-28 10:38:17
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-12 17:14:18
 * @LastEditors: zhangjiajun
 */

import { ExtendsLoad } from "../CCExtends";
/**扩展原型组件 */
@ExtendsLoad(cc.Mat4)
export class Mat4BaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Mat4.prototype)
export class Mat4Extends {

}