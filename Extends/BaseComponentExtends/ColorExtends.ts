import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Color)
export class ColorBaseExtends {
}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Color.prototype)
export class ColorExtends {
}
