/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-28 10:34:49
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-21 17:18:20
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Rect)
export class RectBaseExtends {
    /**
    * 返回两个矩形的重叠矩形，不重叠则返回null
    */
    static overlapRect(r1: cc.Rect, r2: cc.Rect): cc.Rect {
        let xMin = Math.max(r1.xMin, r2.xMin);
        let xMax = Math.min(r1.xMax, r2.xMax);
        let yMin = Math.max(r1.yMin, r2.yMin);
        let yMax = Math.min(r1.yMax, r2.yMax);

        if (xMin > xMax || yMin > yMax) {
            return null;
        }
        return cc.rect(xMin, yMin, xMax - xMin, yMax - yMin);
    }
}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Rect.prototype)
export class RectExtends {
    /**
    * 返回两个矩形的重叠矩形，不重叠则返回null
    * @param targetRect 目标矩形
    */
    static overlapRect(targetRect: cc.Rect): cc.Rect {
        let self = this as any as cc.Rect;
        let xMin = Math.max(self.xMin, targetRect.xMin);
        let xMax = Math.min(self.xMax, targetRect.xMax);
        let yMin = Math.max(self.yMin, targetRect.yMin);
        let yMax = Math.min(self.yMax, targetRect.yMax);
        if (xMin > xMax || yMin > yMax) {
            return null;
        }
        return cc.rect(xMin, yMin, xMax - xMin, yMax - yMin);
    }

    static toSize() {
        let self = this as any as cc.Rect;
        return new cc.Size(self.width, self.height);
    }

    /**
     * 转换vec2
     * @param isPos 是否通过坐标转换, 否则转换宽高
     * @returns 
     */
    static toV2(isPos = true) {
        let self = this as any as cc.Rect;
        if (isPos)
            return new cc.Vec2(self.x, self.y);
        else
            return new cc.Vec2(self.width, self.height);
    }

    static toV3() {
        let self = this as any as cc.Rect;
        return new cc.Vec3(self.x, self.y);
    }

    static toV4() {
        let self = this as any as cc.Rect;
        return new cc.Vec4(self.x, self.y, self.width, self.height);
    }

    /**
     * 返回当前矩形的字符串表示。
     * @returns 当前矩形的字符串表示。
     */
    static print() {
        let self = this as any as cc.Rect;
        return `(${self.x.toFixed(2)}, ${self.y.toFixed(2)}, ${self.width.toFixed(2)}, ${self.height.toFixed(2)})`;
    }

    /**
     * 转换为多边形
     * @param limit 限制
     * @returns 
     */
    static converPolygon() {
        let self = this as any as cc.Rect;
        let rect = [];
        let limit = this.getLimit();
        rect[0].set(limit.xMin, limit.yMin);
        rect[1].set(limit.xMin, limit.yMax);
        rect[2].set(limit.xMax, limit.yMax);
        rect[3].set(limit.xMax, limit.yMin);
        return rect;
    }

    /**获得限制范围 */
    static getLimit() {
        let rect = this as any as cc.Rect;
        let limit = {
            yMax: rect.y + rect.height,
            yMin: rect.y,
            xMax: rect.x + rect.width,
            xMin: rect.x,
        }
        return limit;
    }

    /**
     * 四等分 拿左上角
     * @returns 
     */
    static leftTopEqually() {
        let self = this as any as cc.Rect;
        return new cc.Rect(self.x, self.y + self.height / 2, self.width / 2, self.height / 2);
    }
}
