import { ExtendsLoad } from "../CCExtends";


declare global {
    module cc {
        interface Component {

            /**组件事件, 直接new Map 设置，在onLoad时会自动注册,onDestroy注销*/
            // comEvent: Map<keyof IEventKey, broadcast.Listener<any, any>>;
            // applyComponentsFunction: typeof ComponentEventExtends.applyComponentsFunction;
            // addEventListeners: typeof ComponentEventExtends.addEventListeners;
            // removeEventListeners: typeof ComponentEventExtends.removeEventListeners;

            /***********按钮事件注册******************** */
            // //#region 
            // /**onload时注册事件 */
            // regEvent(): void;
            // /**onDestory取消事件 */
            // unRegEvent(): void;
            // /**
            //  * 当按钮抬起来时触发，只有设置_enabledKeyUp为true生效
            //  * @param ev event
            //  */
            // // onKeyUp(ev: cc..EventKeyboard): void;
            // /**
            //  * 当按钮按下时触发，只有设置_enabledKeyDown为true生效
            //  * @param ev event
            //  */
            // // onKeyDown(ev: cc..EventKeyboard): void;
            // /**鼠标滑轮滚动时 */
            // onMouseWheel(): void;
            // /**鼠标抬起时 */
            // onMouseDown(): void;
            // /**鼠标按下时 */
            // onMouseEnter(): void;
            // /**手指按下时执行 */
            // onTouchStart(): void;
            // /**手指移动时执行 */
            // onTouchMove(): void;
            // /**手指在节点内抬起时执行 */
            // onTouchNodeEnd(): void;
            // /**手指在节点外抬起时执行 */
            // onTouchNodeCancel(): void;
            // /**手指抬起时执行 */
            // onTouchEnd(): void;
            // /**子节点添加时 */
            // onAddChild(): void;
            // /**子节点移除时时 */
            // onRemoveChild(): void;
            // /**节点尺寸改变时 */
            // onSizeChange(): void;

            // /**启用按键抬起按下事件 */
            // eableKeyTouch(): void;
            // /**启用触摸相关的事件 */
            // eableTouch(): void;
            // /**禁止按键抬起按下事件 */
            // disableKeyTouch(): void;
            // /**禁止所有触摸事件 */
            // disableTouch(): void;
            // //#endregion

        }

    }
    module cc.Component {
        interface EventHandler {
            _componentId: string;
            _genCompIdIfNeeded: () => void;
        }
    }
}



/**扩展原型组件 */
@ExtendsLoad(cc.Component)
export class ComponentBaseExtends {
}

/**扩展组件实例方法 */
@ExtendsLoad(cc.Component.prototype)
export class ComponentExtends {
    /**
     * 获取组件的名字 
     * @returns  'nodeName<Sprite>' => Sprite
     */
    static getComName() {
        return this.name.slice(this.name.indexOf('<') + 1, this.name.indexOf('>'))
    }
    /**
    * 获取对应组件的组件名字，并加上$
    * @returns cc.Sprite => $Sprite
    */
    static getSName() { return `$${this.name.match(/<.*>$/)[0].slice(1, -1)}`; }

    /**
    * 异步等待 - cc.Component.scheduleOnce
    * @param seconds 等待组件的时间
    */
    static waitCmpt(seconds: number): Promise<void> {
        let self = this as unknown as cc.Component;
        return new Promise((resolve, reject) => {
            self.scheduleOnce(() => {
                resolve();
            }, seconds);
        });
    }

    //#region  
    /**
    * 通过函数名字调用节点上有该方法的组件上的函数
    * @param node 节点
    * @param funName 函数名
    * @param params 参数
    */
    // static applyComponentsFunction(node: cc.Node, funName: string, params: any) {
    //     for (let i = 0; i < node.components.length; i++) {
    //         let component: any = node.components[i];
    //         let func = component[funName];
    //         if (func) {
    //             func.call(component, params);
    //         }
    //     }
    // }

    /***********关于事件注册***********/
    /**添加 comEvent里的事件， 会在onLoad时自动调用*/
    // static addEventListeners() {
    //     let self = this as any as cc.Component;
    //     self.comEvent?.forEach((func, name) => {
    //         // GM.eventMgr.ins.on(name, func, this);
    //     });
    // }
    /**移除comEvent里的事件 ， 会在onDestory自动调用*/
    // static removeEventListeners() {
    //     let self = this as any as cc.Component;
    //     if (self.comEvent && self.comEvent.size > 0) {
    //         // GM.eventMgr.ins.offAllByContext(this);
    //         self.comEvent?.clear();
    //     }
    // }

    /**注册事件 */
    // static regEvent() {
    //     this.eableKeyTouch();
    //     this.eableTouch();
    // }

    // /**取消注册事件 */
    // static unRegEvent() {
    //     this.disableKeyTouch();
    //     this.disableTouch();
    // }

    // static eableKeyTouch() {
    //     let self = this as unknown as cc.Component;
    //     // self.onKeyUp && cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, self.onKeyUp, this);
    //     // self.onKeyDown && cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, self.onKeyDown, this);
    //     self.onMouseWheel && self.node.on(cc.Node.EventType.MOUSE_WHEEL, self.onMouseWheel, this);
    //     self.onMouseDown && self.node.on(cc.Node.EventType.MOUSE_DOWN, self.onMouseDown, this);
    //     self.onMouseEnter && self.node.on(cc.Node.EventType.MOUSE_ENTER, self.onMouseEnter, this);
    // }

    // static disableKeyTouch() {
    //     let self = this as unknown as cc.Component;
    //     // self.onKeyUp && cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, self.onKeyUp, this);
    //     // self.onKeyDown && cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, self.onKeyDown, this);
    //     self.onMouseWheel && self.node.off(cc.Node.EventType.MOUSE_WHEEL, self.onMouseWheel, this);
    //     self.onMouseDown && self.node.off(cc.Node.EventType.MOUSE_DOWN, self.onMouseDown, this);
    //     self.onMouseEnter && self.node.off(cc.Node.EventType.MOUSE_ENTER, self.onMouseEnter, this);
    // }

    // static eableTouch() {
    //     let self = this as unknown as cc.Component;
    //     self.onTouchStart && self.node.on(cc.Node.EventType.TOUCH_START, self.onTouchStart, this);
    //     self.onTouchMove && self.node.on(cc.Node.EventType.TOUCH_MOVE, self.onTouchMove, this);
    //     self.onTouchNodeEnd && self.node.on(cc.Node.EventType.TOUCH_END, self.onTouchNodeEnd, this);
    //     self.onTouchNodeCancel && self.node.on(cc.Node.EventType.TOUCH_CANCEL, self.onTouchNodeCancel, this);
    //     self.onTouchEnd && self.node.on(cc.Node.EventType.TOUCH_END, self.onTouchEnd, this);
    //     self.onTouchEnd && self.node.on(cc.Node.EventType.TOUCH_CANCEL, self.onTouchEnd, this);

    //     self.onAddChild && self.node.on(cc.Node.EventType.CHILD_ADDED, self.onAddChild, this);
    //     self.onRemoveChild && self.node.on(cc.Node.EventType.CHILD_REMOVED, self.onRemoveChild, this);
    //     self.onSizeChange && self.node.on(cc.Node.EventType.SIZE_CHANGED, self.onSizeChange, this);
    // }

    // static disableTouch() {
    //     let self = this as unknown as cc.Component;
    //     self.onTouchStart && self.node.off(cc.Node.EventType.TOUCH_START, self.onTouchStart, this);
    //     self.onTouchMove && self.node.off(cc.Node.EventType.TOUCH_MOVE, self.onTouchMove, this);
    //     self.onTouchNodeEnd && self.node.off(cc.Node.EventType.TOUCH_END, self.onTouchNodeEnd, this);
    //     self.onTouchNodeCancel && self.node.off(cc.Node.EventType.TOUCH_CANCEL, self.onTouchNodeCancel, this);
    //     self.onTouchEnd && self.node.off(cc.Node.EventType.TOUCH_END, self.onTouchEnd, this);
    //     self.onTouchEnd && self.node.off(cc.Node.EventType.TOUCH_CANCEL, self.onTouchEnd, this);

    //     self.onAddChild && self.node.off(cc.Node.EventType.CHILD_ADDED, self.onAddChild, this);
    //     self.onRemoveChild && self.node.off(cc.Node.EventType.CHILD_REMOVED, self.onRemoveChild, this);
    //     self.onSizeChange && self.node.off(cc.Node.EventType.SIZE_CHANGED, self.onSizeChange, this);
    // }
    //#endregion

}



//添加onload ,ondestory事件控制
// if (!CC_EDITOR) {
// @ts-ignore
let onLoad = cc.Component.prototype.onLoad;
//@ts-ignore
cc.Component.prototype.onLoad = function () {
    let self = this as any as cc.Component;
    // self.addEventListeners?.();
    onLoad?.apply(this, arguments);
}

//@ts-ignore
let onEable = cc.Component.prototype.onEnable;
//@ts-ignore
cc.Component.prototype.onEnable = function () {
    let self = this as any as cc.Component;
    // self.regEvent?.();
    onEable?.apply(this, arguments);
}

//@ts-ignore
let onDisEable = cc.Component.prototype.onDisable;
//@ts-ignore
cc.Component.prototype.onDisable = function () {
    let self = this as any as cc.Component;
    // self.unRegEvent?.();
    onDisEable?.apply(this, arguments);
}

//@ts-ignore
let onDestory = cc.Component.prototype.onDestroy;
//@ts-ignore
cc.Component.prototype.onDestroy = function () {
    let self = this as any as cc.Component;
    // self.removeEventListeners?.();
    onDestory?.apply(this, arguments);
}
// }