/*
* @features: 功能
* @description: 说明
* @Date: 2021-12-31 00:52:35
* @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 14:03:22
 * @LastEditors: judu233
*/

import { ExtendsLoad } from "../CCExtends";
/**扩展原型组件 */
@ExtendsLoad(cc.Widget)
export class WidgetBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Widget.prototype)
export class WidgetExtends {
    static restAllZero() {
        let widget = (this as any as cc.Widget);
        widget.left = widget.right = widget.bottom = widget.top = 0;
        widget.updateAlignment();
    }

    static restAllCenter() {
        let widget = (this as any as cc.Widget);
        widget.horizontalCenter = widget.verticalCenter = 0;
        widget.updateAlignment();
    }
}
