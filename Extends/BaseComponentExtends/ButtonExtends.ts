/*
 * @features: 功能
 * @description: 说明
 * @Date: 2021-10-07 14:21:26
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 13:50:13
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";


enum IButtonHackEvent {
    /** 按钮状态变更 */
    STATE_CHANGE = 'ButtonHackEvent-STATE_CHANGE',
}
enum IButtonState {
    NORMAL = 0,
    HOVER = 1,
    PRESSED = 2,
    DISABLED = 3,
}
window['ButtonHackEvent'] = IButtonHackEvent;
window['ButtonState'] = IButtonState;

declare global {
    enum ButtonHackEvent {
        /** 按钮状态变更 */
        STATE_CHANGE = 'ButtonHackEvent-STATE_CHANGE',
    }
    enum ButtonState {
        NORMAL = 0,
        HOVER = 1,
        PRESSED = 2,
        DISABLED = 3,
    }
}


/**扩展原型组件 */
@ExtendsLoad(cc.Button)
export class ButtonBaseExtends {
}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Button.prototype)
export class ButtoneExtends {

    /**
     * 创建一个Handle事件
     * @param callFunName 方法名
     * @param customEventData 自定义参数
     * @returns handle
     */
    static createInteractionEvent<T extends cc.Component>(callFunName: keyof T, customEventData?: any) {
        let com = this as any as cc.Component;
        let clickHandle = new cc.Component.EventHandler();
        clickHandle.target = com.node;
        clickHandle.component = com[`__classname__`]; //这个脚本的类名
        clickHandle.handler = callFunName as string;
        clickHandle.customEventData = customEventData;
        return clickHandle;
    }

    /**
     * 拷贝按钮上的事件到目标节点
     * @param target 目标节点
     */
    static copyHandleTo(target: cc.Node) {
        let copyBtn = this as any as cc.Button;
        let targetBtn = target.getComponent(cc.Button);
        if (!targetBtn)
            targetBtn = target.addComponent(cc.Button);
        copyBtn.clickEvents.forEach(e => targetBtn.clickEvents.push(e));
    }

    static _applyTransition(state: any) {
        let btn = this as any as cc.Button;
        let transition = btn.transition;
        if (transition === cc.Button.Transition.COLOR) {
            btn['_updateColorTransition'](state);
        } else if (transition === cc.Button.Transition.SPRITE) {
            btn['_updateSpriteTransition'](state);
        } else if (transition === cc.Button.Transition.SCALE) {
            btn['_updateScaleTransition'](state);
        }
        // 状态变更通知
        btn.node.emit(IButtonHackEvent.STATE_CHANGE, state);
    }
}

