/*
 * @features: 功能
 * @description: 说明
 * @Date: 2021-09-13 23:50:25
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-20 16:57:08
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Size)
export class SizeBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Size.prototype)
export class SizeExtends {
    static toV2() {
        let size = this as any as cc.Size;
        return new cc.Vec2(size.width, size.height);
    }
    static toV3() {
        let size = this as any as cc.Size;
        return new cc.Vec3(size.width, size.height);
    }
    static toV4() {
        let size = this as any as cc.Size;
        return new cc.Vec4(size.width, size.height);
    }
    static toRect(x?: number | cc.Vec2, y?: number) {
        let size = this as any as cc.Size;
        if (x == null) {
            return new cc.Rect(0, 0, size.width, size.height);
        }
        if (x instanceof cc.Vec2) {
            return new cc.Rect(x.x, x.y, size.width, size.height);
        } else {
            return new cc.Rect(x, y ?? 0, size.width, size.height);
        }
    }
  
    /**
     * 和指定数相乘
     * @param value 乘数
     * @returns 
     */
    static multiply(value: number | cc.Vec2) {
        let size = this as any as cc.Size;
        if (typeof value == 'number') {
            return new cc.Size(size.width * value, size.height * value);
        } else {
            return new cc.Size(size.width * value.x, size.height * value.y);
        }
    }
}