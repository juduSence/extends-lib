/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-09-29 15:11:54
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 15:02:50
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";
/**扩展原型组件 */
@ExtendsLoad(cc.Director)
export class DirectorBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Director.prototype)
export class DirectorExtends {

}