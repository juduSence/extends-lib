/*
 * @features: 功能
 * @description: 说明
 * @Date: 2021-12-31 23:59:41
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 14:07:21
 * @LastEditors: judu233
 */
import { ExtendsLoad } from "../CCExtends";
 
/**扩展原型组件 */
@ExtendsLoad(cc.Animation)
export class AnimationBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Animation.prototype)
export class AnimationeExtends {
    /**
     *  重置anim动画
     * @param name 要重置的anim上动画的name, 没有就默认当前正在播放的anim;
     * @returns 
     */
    static restAnim(name?: string) {
        let anim = this as any as cc.Animation;
        if (anim.currentClip == null) {
            console.warn(`anim:${anim.name}没有要rest的anim`);
            return;
        }
        name = name ?? anim.currentClip.name;
        anim[`_animator`].setStateTime(anim.getAnimationState(name), 0);
        anim.setCurrentTime(0, name);
        anim.sample(name);
    }

    /**
    * 异步播放指定动画
    * @param name 要播放的动画名称
    * @returns promise
    */
    static asyncPlayAnim(name?: string) {
        let anim = this as any as cc.Animation;
        return new Promise<void>(async resolve => {
            //注册一次性监听，防止触发resolveAfterPromiseResolved错误
            anim.node.active = true;
            console.log(`asyncPlayAnim开始播放动画:${name}`);
            anim.once(cc.Animation.EventType.FINISHED, () => {
                console.log(`asyncPlayAnim播放动画:${name} 完成`);
                resolve();
            });
            anim.play(name);
        })
    }
}