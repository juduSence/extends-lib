/*
* @features: 功能
* @description: 说明
* @Date: 2021-09-28 23:32:47
* @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 15:53:30
 * @LastEditors: judu233
*/

import { ExtendsLoad } from "../CCExtends";

declare global {
    export module cc {
        export interface Label {
            /**主动更新文本 */
            _forceUpdateRenderData(): void;
        }
    }
}

/**扩展原型组件 */
@ExtendsLoad(cc.Label)
export class LabelBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Label.prototype)
export class LabelExtends {
    /**置灰图片*/
    // static setSpMtGray() {
    //     let grayMt = cc.Material.getBuiltinMaterial("2d-gray-sprite");
    //     let sp = this as any as cc.Sprite;

    //     sp.setMaterial(0, grayMt);
    // }

    /**设置图片普通材质 */
    // static setSpMtNormal() {
    //     let normalMt = cc.Material.getBuiltinMaterial('2d-sprite');
    //     let sp = this as any as cc.Sprite;
    //     sp.setMaterial(0, normalMt);
    // }


    /**
     * 计算文本的size
     * @param text 要显示文本内容
     * @param designSize label的设计宽高
     * @param fontSize 字体大小
     * @param lineHeight 行高
     */
    static measureSize(text: string, designSize: cc.Size, fontSize: number, lineHeight: number): cc.Size {
        let lb = this as any as cc.Label;
        if (!lb) {
            let node = new cc.Node();
            lb = node.addComponent(cc.Label);
        }
        lb.fontSize = fontSize;
        lb.lineHeight = lineHeight;
        lb.string = text;

        // 计算宽 
        lb.overflow = cc.Label.Overflow.NONE;
        lb.node.setContentSize(new cc.Size(0, lineHeight));
        lb._forceUpdateRenderData();

        let textWidth = Math.min(lb.node.width, designSize.width);
        textWidth = Math.floor(textWidth / fontSize) * fontSize;
        // 计算高
        lb.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
        lb.node.setContentSize(new cc.Size(textWidth, 0));
        lb._forceUpdateRenderData();
        // lb._updateRenderData(true);
        let textHeight = lb.node.height;
        return new cc.Size(textWidth, textHeight);
    }
}