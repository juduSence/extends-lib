/*
 * @features: 功能
 * @description: 说明
 * @Date: 2022-01-01 00:32:52
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-11-03 23:23:05
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

declare global {
    module cc.Event {
        interface EventTouch {
            target: cc.Node;
            currentTarget: cc.Node;
        }
    }
}

/**扩展事件基类处理 */
@ExtendsLoad(cc.Component.EventHandler)
export class ComponentEventHandleBaseExtends {
    /**
      * 创建一个事件句柄
      * @param node 节点
      * @param comName 组件名字
      * @param callFunName 组件函数string
      * @param customEventData 自定义参数
     * @returns eventHandle
      */
    static createHandle<T extends cc.Component>(com: T, callFunName: keyof T, customEventData?: any) {
        let clickHandle = new cc.Component.EventHandler();
        clickHandle.target = com.node;
        clickHandle.component = cc.js.getClassName(com); //这个脚本的类名
        clickHandle.handler = callFunName as string;
        clickHandle.customEventData = customEventData ?? '';
        clickHandle._componentId = com[`__cid__`];
        return clickHandle;
    }
}
/**扩展事件处理 */
@ExtendsLoad(cc.Component.EventHandler.prototype)
export class ComponentEventHandleExtends {
    /**
     * 返回自身拷贝
     * @returns eventHandle
     */
    static copyHandle() {
        //克隆数据，每个节点获取的都是不同的回调
        let event = this as any as cc.Component.EventHandler
        let hd = new cc.Component.EventHandler();//copy对象
        hd.target = event.target;
        hd.handler = event.handler;
        hd.component = event.component;
        hd._componentId = event._componentId;
        return hd;
    }

    /**
     * 检查自身是否符合规则
     * @returns boolen
     */
    static checkHandle() {
        let event = this as any as cc.Component.EventHandler;
        let target = event.target;
        if (!cc.isValid(target))
            return false;
        event._genCompIdIfNeeded();
        let compType = cc.js._getClassById(event._componentId) as any;
        let comp = target.getComponent(compType);
        if (!cc.isValid(comp))
            return false;
        let handler = comp[event.handler] as Function;
        if (typeof (handler) !== `function`)
            return false;
        if (handler.call(comp, event.customEventData))
            return true;
        return false;
    }

    /**回调组件的方法 */
    static emitAsync<T>(params: any[] = []): T {
        let event = this as any as cc.Component.EventHandler;
        var target = event.target;
        if (!cc.isValid(target)) return;

        event._genCompIdIfNeeded();
        var comp = target.getComponent(cc.js._getClassById(event._componentId));
        if (!cc.isValid(comp)) return;

        var handler = comp[event.handler] as Function;
        if (typeof (handler) !== 'function') return;

        if (event.customEventData != null && event.customEventData !== '') {
            params = params.slice();
            params.push(event.customEventData);
        }
        return handler.apply(comp, params);
    }
}