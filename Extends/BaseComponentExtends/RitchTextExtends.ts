import { ExtendsLoad } from "../CCExtends";
/*
 * @features: 功能
 * @description: 说明
 * @Date: 2021-12-31 00:52:35
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 14:04:41
 * @LastEditors: judu233
 */

/**扩展原型组件 */
@ExtendsLoad(cc.RichText)
export class RichTextBaseExtends {
}
/**扩展组件实例方法 */
@ExtendsLoad(cc.RichText.prototype)
export class RichTexteExtends {
}   
