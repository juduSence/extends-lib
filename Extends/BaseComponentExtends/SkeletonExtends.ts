/*
 * @features: 主要功能
 * @description: 内容说明
 * @Date: 2021-10-09 12:42:45
 * @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-09-26 17:35:14
 * @LastEditors: judu233
 */

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(sp)
export class SkeletonBaseExtends {
}
/**扩展组件实例方法 */
@ExtendsLoad(sp?.Skeleton)
export class SkeletonExtends {

    /**
     * @description 扩展方法
     * @param remotePath 远程资源路径
     * @param name 远程Spine文件名，不再后缀
     * @param completeCallback 完成回调
     * @param isNeedCache 是否需要缓存到本地,如果不需要，每次都会从网络拉取资源,默认都会缓存到本地
     * @param config.retain 远程加载的资源是否驻留在内存中,默认都不驻留内存
     * @example
     * var skeleton = node.addComponent(sp.Skeleton);
     *
     * let path = "https://bc-test1.oss-cn-shenzhen.aliyuncs.com/image/action";
     * let name = "nnoh_v4";
     * skeleton.loadRemoteSkeleton({view : this , path : path, name : name, completeCallback : (data:sp.SkeletonData)=>{
     *    if (data) {
     *        skeleton.animation = 'loop';
     *        skeleton.premultipliedAlpha = false;
     *    }
     * }});
     */
    static loadRemoteSkeleton(config: any) {
        let me = this;
        // if (config.isNeedCache == undefined || config.isNeedCache == null) {
        //     config.isNeedCache = true;
        // }
        // Manager.assetManager.remote.loadSkeleton(config.path, config.name, config.isNeedCache).then((data) => {
        //     setSkeletonSkeletonData(me, config, data as sp.SkeletonData, Resource.Type.Remote);
        // });
    }

    /**
     * @description 加载动画
     * @example
     * action.loadSkeleton({url:"hall/vip/vipAction/vip_10",view:this,completeCallback:(data)=>{
     *	if ( data ){
     *		action.animation = "loop";
     *		action.loop = true;
     *		action.premultipliedAlpha = false;
     *	}
     * }});
     */
    static loadSkeleton(config: any) {
        let me = this;
        // let url = config.url;
        // let bundle = getBundle(config);
        // Manager.cacheManager.getCacheByAsync(url, sp.SkeletonData, bundle).then((data) => {
        //     setSkeletonSkeletonData(me, config, data);
        // });
    }

    /**
       * 
       * @param aniNode 
       * @param url 
       * @param act 
       * @param call 
       * @param clistener 
       * @param scale 
       */
    static reloadSpAnimation(aniNode, url, act, call = null, clistener = null, scale = 1) {
        // aniNode.scale = scale;
        // let reload = function(spData){
        //     let ani = aniNode.getComponent(sp.Skeleton)
        //     ani.skeletonData = spData;
        //     ani.animation = act;
        //     ani.setCompleteListener(clistener);
        //     if (call) {
        //         call(ani);
        //     }
        // }
        // if(resCahe.getSpine(url)){
        //     reload(resCahe.getSpine(url));
        //     return;
        // }
        // cc.loader.loadRes(url, sp.SkeletonData, (err, spData) => {
        //     if (err) {
        //         console.error('spine err: ' + url);
        //         return;
        //     }
        //     resCahe.setSpine(url,spData);
        //     reload(spData);
        // })
    }
}