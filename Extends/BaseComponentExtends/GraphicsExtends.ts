/*
* @features: 扩展画笔
* @description: 说明
* @Date: 2021-09-28 23:32:47
* @Author: judu233(769471424@qq.com)
 * @LastEditTime: 2022-08-14 14:05:47
 * @LastEditors: judu233
*/

import { ExtendsLoad } from "../CCExtends";

/**扩展原型组件 */
@ExtendsLoad(cc.Graphics)
export class GraphicsBaseExtends {

}
/**扩展组件实例方法 */
@ExtendsLoad(cc.Graphics.prototype)
export class GraphicsExtends {
    /**
     * 创建一个圆形
     * @param radius 半径
     * @param color 颜色
     * @returns 
     */
    static createRound(radius = 1, color = new cc.Color(255, 0, 0, 127)): cc.Node {
        let node = new cc.Node();
        let ctx = this as any as cc.Graphics;
        ctx.fillColor = color;
        ctx.circle(0, 0, radius);
        ctx.fill();
        return node;
    }

    /**
     * 创建一个矩形
     * @param size 尺寸大小
     * @param color 颜色色
     * @returns 
     */
    static createRectangle(size = cc.size(10, 10), color = new cc.Color(255, 0, 0, 127)): cc.Node {
        let node = new cc.Node();
        let ctx = this as any as cc.Graphics;
        ctx.fillColor = color;
        ctx.rect(-size.width / 2, -size.height / 2, size.width, size.height);
        ctx.fill();
        node.setContentSize(size);
        return node;
    }

}